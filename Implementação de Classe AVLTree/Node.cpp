/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.cpp
 * Author: vinicius
 * 
 * Created on 31 de Maio de 2018, 08:58
 */

#include <stddef.h>

#include "Node.h"

Node::Node() {
    this->key = 0;
    this->balance = 0;
    this->left = NULL;
    this->right = NULL;
}

Node::Node(int key) {
    this->key = key;
    this->balance = 0;
    this->left = NULL;
    this->right = NULL;
}

Node::~Node() {
    delete this->left;
    delete this->right;
}

