/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AVLTree.cpp
 * Author: vinicius
 * 
 * Created on 31 de Maio de 2018, 09:01
 */

#include <stddef.h>

#include "AVLTree.h"

AVLTree::AVLTree() {
    this->count = 0;
    this->root = NULL;
}

AVLTree::~AVLTree() {
    this->Clear();
}

void AVLTree::AVLify(int array[], int size) {
    for (int i = 0; i < size; i++)
        this->Insert(array[i]);
}

void AVLTree::Clear() {
    this->count = 0;
    delete this->root;
    this->root = NULL;
}

int AVLTree::Size() {
    return (this->count);
}

Node* AVLTree::Search(int key) {

    Node* current = this->root;

    while (current != NULL) {

        if (key > current->key)
            current = current->right; // The value is more to the right
        else if (key < current->key)
            current = current->left; // The value is more to the left
        else
            return current; // We found, we returned the item
    }
    return NULL; // The value was not in the tree
}

void AVLTree::Insert(int key) {

    // For passing by reference
    bool hChanged = false;

    // We call the recursive method to perform the operation
    this->insert(key, this->root, hChanged);
}

void AVLTree::insert(int key, Node* &pA, bool &h) {

    Node* pB = NULL;
    Node* pC = NULL;

    if (pA == NULL) {

        pA = new Node(key);
        this->count++;
        h = true;

    } else if (pA->key == key)
        return;

    else if (key < pA->key) {

        this->insert(key, pA->left, h);

        if (h) {

            switch (pA->balance) {

                case -1:
                    pA->balance = 0;
                    h = false;
                    break;

                case 0:
                    pA->balance = +1;
                    break;

                case +1:
                    pB = pA->left;

                    if (pB->balance == +1) // rotate LL
                        this->rotateLL(pA, pB);
                    else // rotate LR
                        this->rotateLR(pA, pB, pC);

                    pA->balance = 0;
                    h = false;

            } // end switch

        }

    } else if (key > pA->key) {

        this->insert(key, pA->right, h);

        if (h) {

            switch (pA->balance) {

                case +1:
                    pA->balance = 0;
                    h = false;
                    break;

                case 0:
                    pA->balance = -1;
                    break;

                case -1:
                    pB = pA->right;

                    if (pB->balance == -1) // rotate RR
                        this->rotateRR(pA, pB);

                    else // rotate RL
                        this->rotateRL(pA, pB, pC);

                    pA->balance = 0;
                    h = false;

            } // end switch

        }
    }
}

void AVLTree::rotateLL(Node* &pA, Node* &pB) {
    pA->left = pB->right;
    pB->right = pA;
    pA->balance = 0;
    pA = pB;
}

void AVLTree::rotateLR(Node* &pA, Node* &pB, Node* &pC) {
    pC = pB->right;
    pB->right = pC->left;
    pC->left = pB;
    pA->left = pC->right;
    pC->right = pA;

    if (pC->balance == +1)
        pA->balance = -1;
    else
        pA->balance = 0;

    if (pC->balance == -1)
        pB->balance = +1;
    else
        pB->balance = 0;

    pA = pC;
    pC->balance = 0;
}

void AVLTree::rotateRR(Node* &pA, Node* &pB) {
    pA->right = pB->left;
    pB->left = pA;
    pA->balance = 0;
    pA = pB;
}

void AVLTree::rotateRL(Node* &pA, Node* &pB, Node* &pC) {
    pC = pB->left;
    pB->left = pC->right;
    pC->right = pB;
    pA->right = pC->left;
    pC->left = pA;

    if (pC->balance == -1)
        pA->balance = +1;
    else
        pA->balance = 0;

    if (pC->balance == +1)
        pB->balance = -1;
    else
        pB->balance = 0;

    pA = pC;
    pC->balance = 0;
}

bool AVLTree::Remove(int key) {

    // For passing by reference
    bool hChanged = false;

    // We call the recursive method to perform the remove operation
    if ((this->remove(key, this->root, hChanged))) {
        this->count--;
        return true;
    } else
        return false;
}

bool AVLTree::remove(int key, Node* &p, bool &h) {

    Node* q = NULL;

    if (p == NULL)
        return false;

    if (key < p->key) {

        this->remove(key, p->left, h);
        if (h)
            this->BalanceL(p, h);

    } else {

        if (key > p->key) {

            this->remove(key, p->right, h);
            if (h)
                this->BalanceR(p, h);

        } else {

            q = p;

            if (q->right == NULL) {
                p = q->left;
                h = true;
            } else {

                if (q->left == NULL) {
                    p = q->right;
                    h = true;
                } else {

                    this->DelMin(q, q->right, h);
                    if (h)
                        this->BalanceR(p, h);
                }

                q = NULL;
            }
        }

    }
    return true;
}

void AVLTree::BalanceL(Node* &pA, bool &h) {

    Node* pB = NULL;
    Node* pC = NULL;
    int balB, balC;

    // the left subtree has shrunk
    switch (pA->balance) {

        case +1:
            pA->balance = 0;
            break;

        case 0:
            pA->balance = -1;
            h = false;
            break;

        case -1:
            pB = pA->right;
            balB = pB->balance;

            if (balB <= 0) { // rotate RR

                pA->right = pB->left;
                pB->left = pA;
                if (balB == 0) {
                    pA->balance = -1;
                    pB->balance = +1;
                    h = false;
                } else {
                    pA->balance = 0;
                    pB->balance = 0;
                }
                pA = pB;

            } else { // rotate RL

                pC = pB->left;
                balC = pC->balance;
                pB->left = pC->right;
                pC->right = pB;
                pA->right = pC->left;
                pC->left = pA;

                if (balC == -1)
                    pA->balance = +1;
                else
                    pA->balance = 0;
                if (balC == +1)
                    pB->balance = -1;
                else
                    pB->balance = 0;

                pA = pC;
                pC->balance = 0;
            }

    } // end switch
}

void AVLTree::BalanceR(Node* &pA, bool &h) {

    Node* pB = NULL;
    Node* pC = NULL;
    int balB, balC;

    // the right subtree has shrunk
    switch (pA->balance) {

        case -1:
            pA->balance = 0;
            break;

        case 0:
            pA->balance = +1;
            h = false;
            break;

        case +1:
            pB = pA->left;
            balB = pB->balance;

            if (balB >= 0) { // rotate LL

                pA->left = pB->right;
                pB->right = pA;

                if (balB == 0) {
                    pA->balance = +1;
                    pB->balance = -1;
                    h = false;
                } else {
                    pA->balance = 0;
                    pB->balance = 0;
                }
                pA = pB;
                
            } else { // rotate LR

                pC = pB->right;
                balC = pC->balance;
                pB->right = pC->left;
                pC->left = pB;
                pA->left = pC->right;
                pC->right = pA;

                if (balC == +1)
                    pA->balance = -1;
                else
                    pA->balance = 0;

                if (balC == -1)
                    pB->balance = +1;
                else
                    pB->balance = 0;

                pA = pC;
                pC->balance = 0;
            }
    } // end switch
}

void AVLTree::DelMin(Node* &q, Node* &r, bool &h) {

    if (r->left != NULL) {

        this->DelMin(q, r->left, h);
        if (h)
            this->BalanceL(r, h);

    } else {
        q->key = r->key;
        q = r;
        r = r->right;
        h = true;
    }
}

void AVLTree::PrintAVLTree() {
    if (this->root != NULL)
        this->print(this->root);
    else
        cout << "Empty Tree" << endl;
}

void AVLTree::print(Node* &node) {

    if (node != NULL) {
        this->print(node->left);
        this->visit(node);
        this->print(node->right);
    }
}

void AVLTree::visit(Node*& node) {
    cout << node->key << ", ";
}

