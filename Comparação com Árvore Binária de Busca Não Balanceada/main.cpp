/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: vinicius
 *
 * Created on 31 de Maio de 2018, 08:57
 */

#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include <time.h>

#include "AVLTree.h"
#include "BinaryTree.h"

using namespace std;

// Constant sizes
int const SIZE_10 = 10;
int const SIZE_100 = 100;
int const SIZE_500 = 500;
int const SIZE_1000 = 1000;

// Binary tree and AVL tree for testing
BinaryTree* binaryTree;
AVLTree* avlTree;

/**
 * returns if the value V passed by parameter exists in the array
 * @param array[]
 * @param array size
 * @param v
 * @return 
 */
bool Exist(int array[], int size, int v) {
    for (int i = 0; i < size; i++) {
        if (array[i] == v)
            return true;
    }
    return false;
}

/**
 * receives a vector containing n elements, whose values​vary between init and
 * size. Values are not repeated and are arranged randomly in the vector.
 * the (init) parameter indicates whether the random numbers should go from
 * (0 to size-1) or from (1 to size)
 * @param array
 * @param init
 * @param size
 */
void GeneratorRandomArray(int array[], int init, int size) {
    srand(time(NULL));

    int v;
    for (int i = 0; i < size; i++) {
        v = (rand() % ((size - 1) - 0 + 1) + init);

        while (Exist(array, i, v)) {
            v = (rand() % ((size - 1) - 0 + 1) + init);
        }
        array[i] = v;
    }
}

/**
 * Prints on the screen the dynamically created vector
 * @param array
 * @param array size
 */
void PrintArray(int array[], int size) {
    for (int i = 0; i < size; i++)
        cout << " array[" << i << "] = " << array[i] << endl;
}

/**
 * Computes the insertion time of N elements in the binary tree. 
 * Returns the calculated time in milliseconds.
 * @param elementsToInsert[]
 * @param N
 * @return 
 */
double CalculateInsertBinaryTree(int elementsToInsert[], int N) {
    clock_t tInit, tEnd;

    tInit = clock();
    for (int i = 0; i < N; i++)
        binaryTree->Insert(elementsToInsert[i]);
    tEnd = clock();

    return (double (tEnd - tInit) * 1000.0 / CLOCKS_PER_SEC);
}

/**
 * Computes the insertion time of N elements in the AVL tree. 
 * Returns the calculated time in milliseconds.
 * @param elementsToInsert[]
 * @param N
 * @return 
 */
double CalculateInsertAVLTree(int elementsToInsert[], int N) {
    clock_t tInit, tEnd;

    tInit = clock();
    for (int i = 0; i < N; i++)
        avlTree->Insert(elementsToInsert[i]);
    tEnd = clock();

    return (double (tEnd - tInit) * 1000.0 / CLOCKS_PER_SEC);
}

/**
 * Computes the remotion time of N elements in the Binary tree. 
 * Returns the calculated time in milliseconds.
 * @param elementsToInsert[]
 * @param indexesToRemove[]
 * @param N
 * @return 
 */
double CalculateRemoveBinaryTree(int elementsToInsert[], int indexesToRemove[], int N) {
    clock_t tInit, tEnd;

    tInit = clock();
    for (int i = 0; i < N; i++)
        binaryTree->Remove(elementsToInsert[indexesToRemove[i]]);
    tEnd = clock();

    return (double (tEnd - tInit) * 1000.0 / CLOCKS_PER_SEC);
}

/**
 * Computes the remotion time of N elements in the AVL tree. 
 * Returns the calculated time in milliseconds.
 * @param elementsToInsert[]
 * @param indexesToRemove[]
 * @param N
 * @return 
 */
double CalculateRemoveAVLTree(int elementsToInsert[], int indexesToRemove[], int N) {
    clock_t tInit, tEnd;

    tInit = clock();
    for (int i = 0; i < N; i++)
        avlTree->Remove(elementsToInsert[indexesToRemove[i]]);
    tEnd = clock();

    return (double (tEnd - tInit) * 1000.0 / CLOCKS_PER_SEC);
}

/**
 * returns an array with the mean and variance calculation of inertia and removal
 * in BinaryTree, where:
 * 
 * position 0 insertion average
 * position 1 mean of removal
 * position 2 insertion variance
 * position 3 removal variance
 * 
 * @param N (number of replicates)
 * @param size (vector size - 10 - 100 - 500 - 1000)
 * @param elementsToInsert (array of elements)
 * @param indexesToRemove (array with removal positions)
 * @return
 */
double* TimeBinaryTree(int N, int size, int elementsToInsert[], int indexesToRemove[]) {

    double* timeInsert = new double[N];
    double* timeRemove = new double[N];
    double averageInsert;
    double averageRemove;
    double* averageAndVariance = new double[4];

    /************************** Average **************************/
    for (int i = 0; i < N; i++) {
        timeInsert[i] += CalculateInsertBinaryTree(elementsToInsert, size);
        timeRemove[i] += CalculateRemoveBinaryTree(elementsToInsert, indexesToRemove, size);
    }

    for (int i = 0; i < N; i++) {
        averageInsert += timeInsert[i];
        averageRemove += timeRemove[i];
    }

    averageInsert /= N;
    averageRemove /= N;

    averageAndVariance[0] = averageInsert; // average to insert
    averageAndVariance[1] = averageRemove; // average to remove

    /************************** Variance **************************/
    for (int i = 0; i < N; i++) {
        averageAndVariance[2] += (timeInsert[i] - averageInsert)*(timeInsert[i] - averageInsert);
        averageAndVariance[3] += (timeRemove[i] - averageRemove)*(timeRemove[i] - averageRemove);
    }

    averageAndVariance[2] /= N;
    averageAndVariance[3] /= N;

    return averageAndVariance;
}

/**
 * returns an array with the mean and variance calculation of inertia and removal
 * in AVLTree, where:
 * 
 * position 0 insertion average
 * position 1 mean of removal
 * position 2 insertion variance
 * position 3 removal variance
 * 
 * @param N (number of replicates)
 * @param size (vector size - 10 - 100 - 500 - 1000)
 * @param elementsToInsert (array of elements)
 * @param indexesToRemove (array with removal positions)
 * @return
 */
double* TimeAVLTree(int N, int size, int elementsToInsert[], int indexesToRemove[]) {

    double* timeInsert = new double[N];
    double* timeRemove = new double[N];
    double averageInsert;
    double averageRemove;
    double* averageAndVariance = new double[4];

    /************************** Average **************************/
    for (int i = 0; i < N; i++) {
        timeInsert[i] += CalculateInsertAVLTree(elementsToInsert, size);
        timeRemove[i] += CalculateRemoveAVLTree(elementsToInsert, indexesToRemove, size);
    }

    for (int i = 0; i < N; i++) {
        averageInsert += timeInsert[i];
        averageRemove += timeRemove[i];
    }

    averageInsert /= N;
    averageRemove /= N;

    averageAndVariance[0] = averageInsert; // average to insert
    averageAndVariance[1] = averageRemove; // average to remove

    /************************** Variance **************************/
    for (int i = 0; i < N; i++) {
        averageAndVariance[2] += (timeInsert[i] - averageInsert)*(timeInsert[i] - averageInsert);
        averageAndVariance[3] += (timeRemove[i] - averageRemove)*(timeRemove[i] - averageRemove);
    }

    averageAndVariance[2] /= N;
    averageAndVariance[3] /= N;

    return averageAndVariance;
}

/*
 * main method for the tests 
 */
int main() {

    system("clear");

    binaryTree = new BinaryTree();
    avlTree = new AVLTree();
    int* elementsToInsert = NULL;
    int* indexesToRemove = NULL;
    double* averageAndVarianceBinaryTree = NULL;
    double* averageAndVarianceAVLTree = NULL;
    int N = 20;


    /************************** Calculation with 10 **************************/
    elementsToInsert = new int[SIZE_10];
    indexesToRemove = new int[SIZE_10];

    GeneratorRandomArray(elementsToInsert, 1, SIZE_10);
    GeneratorRandomArray(indexesToRemove, 0, SIZE_10);

    averageAndVarianceBinaryTree = TimeBinaryTree(N, SIZE_10, elementsToInsert, indexesToRemove);
    averageAndVarianceAVLTree = TimeBinaryTree(N, SIZE_10, elementsToInsert, indexesToRemove);

    cout << endl << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐         TIME - 10 ELEMENTS        ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Average Insertion BinaryTree: " << averageAndVarianceBinaryTree[0] << endl;
    cout << " ➸ Average Insertion AVLTree: " << averageAndVarianceAVLTree[0] << endl;

    cout << "\n ➸ Average Remotion BinaryTree: " << averageAndVarianceBinaryTree[1] << endl;
    cout << " ➸ Average Remotion AVLTree: " << averageAndVarianceAVLTree[1] << endl;

    cout << "\n ➸ Variance Insertion BinaryTree: " << averageAndVarianceBinaryTree[2] << endl;
    cout << " ➸ Variance Insertion AVLTree: " << averageAndVarianceAVLTree[2] << endl;

    cout << "\n ➸ Variance Remotion BinaryTree: " << averageAndVarianceBinaryTree[3] << endl;
    cout << " ➸ Variance Remotion AVLTree: " << averageAndVarianceAVLTree[3] << endl;


    /************************** Calculation with 100 **************************/
    elementsToInsert = new int[SIZE_100];
    indexesToRemove = new int[SIZE_100];

    GeneratorRandomArray(elementsToInsert, 1, SIZE_100);
    GeneratorRandomArray(indexesToRemove, 0, SIZE_100);

    averageAndVarianceBinaryTree = TimeBinaryTree(N, SIZE_100, elementsToInsert, indexesToRemove);
    averageAndVarianceAVLTree = TimeBinaryTree(N, SIZE_100, elementsToInsert, indexesToRemove);

    cout << endl << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐        TIME - 100 ELEMENTS        ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Average Insertion BinaryTree: " << averageAndVarianceBinaryTree[0] << endl;
    cout << " ➸ Average Insertion AVLTree: " << averageAndVarianceAVLTree[0] << endl;

    cout << "\n ➸ Average Remotion BinaryTree: " << averageAndVarianceBinaryTree[1] << endl;
    cout << " ➸ Average Remotion AVLTree: " << averageAndVarianceAVLTree[1] << endl;

    cout << "\n ➸ Variance Insertion BinaryTree: " << averageAndVarianceBinaryTree[2] << endl;
    cout << " ➸ Variance Insertion AVLTree: " << averageAndVarianceAVLTree[2] << endl;

    cout << "\n ➸ Variance Remotion BinaryTree: " << averageAndVarianceBinaryTree[3] << endl;
    cout << " ➸ Variance Remotion AVLTree: " << averageAndVarianceAVLTree[3] << endl;


    /************************** Calculation with 500 **************************/
    elementsToInsert = new int[SIZE_500];
    indexesToRemove = new int[SIZE_500];

    GeneratorRandomArray(elementsToInsert, 1, SIZE_500);
    GeneratorRandomArray(indexesToRemove, 0, SIZE_500);

    averageAndVarianceBinaryTree = TimeBinaryTree(N, SIZE_500, elementsToInsert, indexesToRemove);
    averageAndVarianceAVLTree = TimeBinaryTree(N, SIZE_500, elementsToInsert, indexesToRemove);

    cout << endl << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐        TIME - 500 ELEMENTS        ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Average Insertion BinaryTree: " << averageAndVarianceBinaryTree[0] << endl;
    cout << " ➸ Average Insertion AVLTree: " << averageAndVarianceAVLTree[0] << endl;

    cout << "\n ➸ Average Remotion BinaryTree: " << averageAndVarianceBinaryTree[1] << endl;
    cout << " ➸ Average Remotion AVLTree: " << averageAndVarianceAVLTree[1] << endl;

    cout << "\n ➸ Variance Insertion BinaryTree: " << averageAndVarianceBinaryTree[2] << endl;
    cout << " ➸ Variance Insertion AVLTree: " << averageAndVarianceAVLTree[2] << endl;

    cout << "\n ➸ Variance Remotion BinaryTree: " << averageAndVarianceBinaryTree[3] << endl;
    cout << " ➸ Variance Remotion AVLTree: " << averageAndVarianceAVLTree[3] << endl;


    /************************** Calculation with 1000 **************************/
    elementsToInsert = new int[SIZE_1000];
    indexesToRemove = new int[SIZE_1000];

    GeneratorRandomArray(elementsToInsert, 1, SIZE_1000);
    GeneratorRandomArray(indexesToRemove, 0, SIZE_1000);

    averageAndVarianceBinaryTree = TimeBinaryTree(N, SIZE_1000, elementsToInsert, indexesToRemove);
    averageAndVarianceAVLTree = TimeBinaryTree(N, SIZE_1000, elementsToInsert, indexesToRemove);

    cout << endl << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << "▐       TIME - 1000 ELEMENTS        ▐" << endl;
    cout << " ▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬" << endl;
    cout << " ➸ Average Insertion BinaryTree: " << averageAndVarianceBinaryTree[0] << endl;
    cout << " ➸ Average Insertion AVLTree: " << averageAndVarianceAVLTree[0] << endl;

    cout << "\n ➸ Average Remotion BinaryTree: " << averageAndVarianceBinaryTree[1] << endl;
    cout << " ➸ Average Remotion AVLTree: " << averageAndVarianceAVLTree[1] << endl;

    cout << "\n ➸ Variance Insertion BinaryTree: " << averageAndVarianceBinaryTree[2] << endl;
    cout << " ➸ Variance Insertion AVLTree: " << averageAndVarianceAVLTree[2] << endl;

    cout << "\n ➸ Variance Remotion BinaryTree: " << averageAndVarianceBinaryTree[3] << endl;
    cout << " ➸ Variance Remotion AVLTree: " << averageAndVarianceAVLTree[3] << endl;
    cout << "\n\n\n";


    delete binaryTree;
    delete avlTree;
    delete elementsToInsert;
    delete indexesToRemove;
    delete averageAndVarianceBinaryTree;
    delete averageAndVarianceAVLTree;

    return 0;
}

